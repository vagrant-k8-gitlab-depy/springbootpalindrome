FROM maven:3.5.2-jdk-8-alpine AS MAVEN_BUILD

MAINTAINER Charles Njiru

COPY pom.xml /build/

COPY src /build/src/

WORKDIR /build/

RUN mvn package

# For Java 8, try this
# FROM openjdk:8-jdk-alpine

FROM openjdk:8-jre-alpine

WORKDIR /app

COPY --from=MAVEN_BUILD /build/target/palindrome-0.0.1-SNAPSHOT.jar /app/

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "palindrome-0.0.1-SNAPSHOT.jar"]