# Spring boot palindrome api

This is a simple spring boot application that accept a word and returns a status indicating 
if the word is a palindrome or not.

## Getting Started

*The application will be deployed to a kurbenetes cluster once the setup is complete.
 For any new commit to the master branch a git runner will execute the .gitlab-ci.yaml to execute
 the defined pipeline.

*To run the app on a container you can run the commands below

### Pre-requisites

#### Windows
1. Downlad Docker for windows from https://hub.docker.com/editions/community/docker-ce-desktop-windows/

2. Install Docker
   ```
    Open the .exe file once it has completed downloading and follow the the installation instructions
   ```
#### Linux
3. Install Docker
   ```
    $ apt update
    $ apt-get install \
       apt-transport-https \
       ca-certificates \
       curl \
       gnupg-agent \
       software-properties-common >/dev/null 2>&1

    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    
    $ udo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) \
        stable"

    $ sudo apt-get update
    $ sudo apt-get install docker-ce docker-ce-cli containerd.io
 
4. Install Java 8 and above

5. Install Maven

6. Pull the app from the repo below
   ```
    $ https://gitlab.com/vagrant-k8-gitlab-depy/springbootpalindrome.git   
      and navigate to the applications directory i.e cd springbootpalindrome

7. Build the image 
   ```
    $ docker build  -t palindrome .

8. Run the container
   ```
    $ docker container run -it  --name palindromeapi -p 8080:8080 -d palindrome 

    $ You can access the application on http://localhost:8080/api/v1/palindrome/mam
